package nz.ac.ara.sjb.game_model;

public class Player {
	
	
	protected int[] playerLocation;
	protected String playerLocationPattern;
	
	private boolean playerMovement;
	private boolean blankSpace;
	private boolean inGrid;
	private boolean moveVer;
	private boolean moveHoz;
	private boolean moveColour;
	private boolean moveShape;
	
	public Player() {
		this.playerMovement = false;
	}
	
	private void checkNextLocation() {
		
	}
	
	public boolean checkMoveHoz() {

		return moveHoz;
	}
		
	public boolean checkMoveVer() {
		
		return moveVer;
	}
	
	public boolean checkBlankSpace(){

		return blankSpace;
	}
	
	public boolean checkInGridSpace(){
		
		return inGrid;
	}


	public boolean checkMoveColour() {
		
		return moveColour;
	}
	
	public boolean checkMoveShape() {
		//if(playerLocationPattern){
			//moveShape = true
		//}
		return moveShape;
	}
	
		
	public int[] updatePlayerLocation() {
	
		return playerLocation;
	}
	
	public boolean movePlayer() {
		if(checkMoveVer() == true && checkMoveHoz()== true){
			if(checkInGridSpace() == true && checkBlankSpace() == true) {
				 if(checkMoveShape() == true && checkMoveColour() == true) {
					 playerMovement = true;
				 }else{
					 //System.out.print("Colour or Shape ERROR")
				 }
			 }else{
				 //System.out.print("IN GRID OR BLANK SPACE ERROR")
			 }
		}else {
			//System.out.print("Move HOR or VER ERROR")
		}
		return playerMovement;
	}
	
	public void getPlayerLocation(int[] tempLocation) {
		tempLocation = playerLocation;
	}

	public void setMovePlayer() {
		
	}
}
