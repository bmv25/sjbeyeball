package nz.ac.ara.sjb.game_model;

public enum Colour {
	RED("r"),
	GREEN("g"),
	BLUE("b"),
	YELLOW("y"),
	BLANK("_");
	
	private String colour;
	
	Colour( String colour  ) {
		this.colour = colour;
	}
	
	public String id() {
		return colour;
	}
}
