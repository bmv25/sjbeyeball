package nz.ac.ara.sjb.game_model;

public enum MoveMoment {
	UP(1,0),
	RIGHT(0,1),
	LEFT(1,0),
	DOWN(0,0);
	
	public int xMove;
	public int yMove;
	
	private MoveMoment(int x, int y) {
		xMove = x;
		yMove = y;
	}
	
	public String toString() {
		String moveMoment = super.toString();
		moveMoment =  moveMoment.substring(0, 1) + moveMoment.substring(1).toLowerCase();
		moveMoment = moveMoment + " " + xMove + "," + yMove;
		return moveMoment;
	}
}
