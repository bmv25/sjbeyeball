package nz.ac.ara.sjb.game_model;

public enum Shape {
	FLOWER("@"),
	CROSS("+"),
	STAR("*"),
	DIAMOND("^"),
	BLANK("-");
	
	private String shape;
	
	Shape( String shape ) {
		this.shape = shape;
	}
	
	public String identifier() {
		return shape;
	}
}
