package nz.ac.ara.sjb.game_model;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Loader {
	
	
	Scanner sc = null;
	
	ArrayList<Tile> level = new ArrayList<Tile>();
	
	//protected Tile[][] level;
	protected int[][] levelSize = new int[6][6];
	protected int[][] goal = new int[3][0];
	protected int[][] player = new int[2][5];

	
	public void setMap(int row, int col) {
		
		if(row == 0 ) {
			if(col == 3) {
				level.add(new Tile(Colour.RED, Shape.FLOWER));
			}else {
				level.add(new Tile(Colour.BLANK, Shape.BLANK));
			}	
		}else if(row == 1) {
			if(col == 1) {
				level.add(new Tile(Colour.BLUE, Shape.CROSS));
			}else if(col == 2) {
				level.add(new Tile(Colour.YELLOW, Shape.FLOWER));
			}else if(col == 3) {
				level.add(new Tile(Colour.YELLOW, Shape.DIAMOND));
			}else if(col == 4) {
				level.add(new Tile(Colour.GREEN, Shape.CROSS));
			}else {
				level.add(new Tile(Colour.BLANK, Shape.BLANK));
			}
		}else if(row == 2) {
			if(col == 1) {
				level.add(new Tile(Colour.GREEN, Shape.FLOWER));
			}else if(col == 2) {
				level.add(new Tile(Colour.RED, Shape.STAR));
			}else if(col == 3) {
				level.add(new Tile(Colour.GREEN, Shape.STAR));
			}else if(col == 4) {
				level.add(new Tile(Colour.YELLOW, Shape.DIAMOND));
			}else {
				level.add(new Tile(Colour.BLANK, Shape.BLANK));
			}		
		}else if(row == 3) {
			
			if(col == 1) {
				level.add(new Tile(Colour.RED, Shape.FLOWER));
			}else if(col == 2) {
				level.add(new Tile(Colour.BLUE, Shape.FLOWER));
			}else if(col == 3) {
				level.add(new Tile(Colour.RED, Shape.STAR));
			}else if(col == 4) {
				level.add(new Tile(Colour.GREEN, Shape.FLOWER));
			}else {
				level.add(new Tile(Colour.BLANK, Shape.BLANK));
			}
		}else if(row == 4) {
			if(col == 1) {
				level.add(new Tile(Colour.BLUE, Shape.STAR));
			}else if(col == 2) {
				level.add(new Tile(Colour.RED, Shape.DIAMOND));
			}else if(col == 3) {
				level.add(new Tile(Colour.BLUE, Shape.FLOWER));
			}else if(col == 4) {
				level.add(new Tile(Colour.BLUE, Shape.DIAMOND));
			}else {
				level.add(new Tile(Colour.BLANK, Shape.BLANK));
			}
		}else if(row == 5 ) {
			if(col == 2) {
				level.add(new Tile(Colour.BLUE, Shape.DIAMOND));
			}else {
				level.add(new Tile(Colour.BLANK, Shape.BLANK));
			}
		}else {
			level.add(new Tile(Colour.BLANK, Shape.BLANK));
		}
		
	}
	
	
	 public void printMap() { 
        for (int row = 0; row < levelSize.length; row++) {
            for (int col = 0; col < levelSize[row].length; col++) {	
            	setMap(row, col);	
            }
            //System.out.println();
        }
        System.out.print(level);
	}
	
	
	public void getFileData(String location) {
		
		
		String filelocation = location;
		File file = new File(filelocation);
		try {
			sc = new Scanner(file);
			
		} catch (FileNotFoundException e1) {
			System.out.println("ERROR reading file can't be found");
			e1.printStackTrace();
		}

		while(sc.hasNextLine()){
			String lines = sc.nextLine();
			String[] line = sc.nextLine().split(",");
			System.out.print(lines );
			int row = 0;
			int col = 0;
			for (row = 0; row < line.length; row++) {
				 for (col = 0; col < line[row].length(); col++) {
					

				    
				 }
			}
			System.out.println(row + " " + col);
		}
		sc.close(); 
	}
	
	public Tile[][] getMap() {
		//return map;
		return null;
	}

	public int[][] getGoal() {
		return goal;
	}

	public void getPlayer() {
		for(int[] i: player) {
			System.out.println(i);
			
		}
	}
	

}
