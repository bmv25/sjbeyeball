package nz.ac.ara.sjb.game_model;

public class Tile {
	protected Colour colour;
	protected Shape shape;
	
	private String tile;
	
	
	public Tile(Colour colour, Shape shape) {
		this.colour = colour;
		this.shape = shape;
		this.tile = colour.id() + shape.identifier();
	}
	
	public String toString(){
	    return tile;
	}
}
