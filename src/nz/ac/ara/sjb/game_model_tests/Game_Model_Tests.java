package nz.ac.ara.sjb.game_model_tests;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.*;

import nz.ac.ara.sjb.game_model.*;

class Game_Model_Tests {
	
	Player user;
	Level lvl;
	Loader load;
	
	

	//there needs to be 41 unit
	/*
	@Test
	void test() {
		fail("Not yet implemented");
	}
		
	@DisplayName("Test 0")
	@Test
	public void testExceptionIsThrown() {
		assertThrows(IllegalArgumentException.class, () -> {PrimeDetector.isPrime(0);});
	}
	*/
	@DisplayName("Test 01: PLAYER: CHECK NEXT LOCATION")
	@Test
	void test01() {
		user = new Player();
		int[] player = user.checkNextLocation();
		int[] expected = {2,5};
		int[] actual = playerLocation;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 02: PLAYER: Boolean MOVE HOZ ")
	@Test
	void test02() {
		user = new Player();
		boolean test_player_movement = user.checkMoveHoz();
		boolean expected = true;
		boolean actual = user.moveHoz;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 03: PLAYER: Boolean MOVE VER")
	@Test
	void test03() {
		user = new Player();
		boolean test_player_movement = user.checkMoveVer();
		boolean expected = true;
		boolean actual = test_player_movement;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 04: PLAYER: Boolean BLANK SPACE")
	@Test
	void test04() {
		user = new Player();
		boolean test_player_movement = user.checkBlankSpace();
		boolean expected = true;
		boolean actual = test_player_movement;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 05: PLAYER: Boolean IN GRID SPACE")
	@Test
	void test05() {
		user = new Player();
		boolean test_player_movement = user.checkInGridSpace();
		boolean expected = true;
		boolean actual = test_player_movement;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 06: PLAYER: Boolean CHECK MOVE COLOUR")
	@Test
	void test06() {
		user = new Player();
		boolean test_player_movement = user.checkMoveColour();
		boolean expected = true;
		boolean actual = test_player_movement;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 07: PLAYER: Boolean CHECK MOVE SHAPE")
	@Test
	void test07() {
		user = new Player();
		boolean test_player_movement = user.checkMoveShape();
		boolean expected = true;
		boolean actual = test_player_movement;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}	
	
	@DisplayName("Test 08: PLAYER: ENUM: Up")
	@Test
	void test08() {
		String move = MoveMoment.UP.toString();
		String expected = "Up 1,0";
		String actual = move;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 09: PLAYER: ENUM: Right ")
	@Test
	void test09() {
		String move = MoveMoment.RIGHT.toString();
		String expected = "Right 0,1";
		String actual = move;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 10: PLAYER: ENUM: Left")
	@Test
	void test10() {
		String move = MoveMoment.LEFT.toString();
		String expected = "Left 1,0";
		String actual = move;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 11: PLAYER: ENUM: Down")
	@Test
	void test11() {
		String move = MoveMoment.DOWN.toString();
		String expected = "Down 0,0";
		String actual = move;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 12: ENUM: SHAPE: Flower")
	@Test
	void test12() {
		String shape = Shape.FLOWER.identifier();
		String expected = "@";
		String actual = shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 13) ENUM: SHAPE: cross")
	@Test
	void test13() {
		String shape = Shape.CROSS.identifier();
		String expected = "+";
		String actual = shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 14: ENUM: SHAPE: star")
	@Test
	void test14() {
		String shape = Shape.STAR.identifier();
		String expected = "*";
		String actual = shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 15: ENUM: SHAPE: Diamonds")
	@Test
	void test15() {
		String shape = Shape.DIAMOND.identifier();
		String expected = "^";
		String actual = shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 16: ENUM: SHAPE: blank")
	@Test
	void test16() {
		String shape = Shape.BLANK.identifier();
		String expected = "-";
		String actual = shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 17: ENUM COLOUR: red ")
	@Test
	void test17() {
		String colour = Colour.RED.id();
		String expected = "r";
		String actual = colour;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 18: ENUM COLOUR: green ")
	@Test
	void test18() {
		String colour = Colour.GREEN.id();
		String expected = "g";
		String actual = colour;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 19: ENUM COLOUR: blue ")
	@Test
	void test19() {
		String colour = Colour.BLUE.id();
		String expected = "b";
		String actual = colour;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 20: ENUM COLOUR: yellow ")
	@Test
	void test20() {
		String colour = Colour.YELLOW.id();
		String expected = "y";
		String actual = colour;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 21: ENUM COLOUR: blank ")
	@Test
	void test21() {
		String colour = Colour.BLANK.id();
		String expected = "_";
		String actual = colour;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 22: LEVEL: ENUM CHECKER: star + red ")
	@Test
	void test22() {
		lvl = new Level();
		String colour = Colour.RED.id();
		String shape = Shape.STAR.identifier();
		String expected = shape + colour;
		String actual = lvl.setGridPattern("*", "r");;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 23: LEVEL: ENUM CHECKER: cross + green ")
	@Test
	void test23() {
		lvl = new Level();
		String colour = Colour.GREEN.id();
		String shape = Shape.CROSS.identifier();
		String expected = shape + colour;
		String actual = lvl.setGridPattern("+", "g");;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 24: LEVEL: ENUM CHECKER: diamond + blue ")
	@Test
	void test24() {
		lvl = new Level();
		String colour = Colour.BLUE.id();
		String shape = Shape.DIAMOND.identifier();
		String expected = shape + colour;
		String actual = lvl.setGridPattern("^", "b");
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 25: LEVEL: ENUM CHECKER: flower + yellow ")
	@Test
	void test25() {
		lvl = new Level();
		String colour = Colour.YELLOW.id();
		String shape = Shape.FLOWER.identifier();
		String expected = shape + colour;
		String actual = lvl.setGridPattern("@", "y");
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	
	

	
	
	@DisplayName("Test 26: LEVEL: ")
	@Test
	void test26() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 27: LEVEL: ")
	@Test
	void test27() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 28: LEVEL: ")
	@Test
	void test28() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 29: LEVEL: ")
	@Test
	void test29() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 30: LEVEL: ")
	@Test
	void test30() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 31: LEVEL: ")
	@Test
	void test31() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 32: LEVEL: ")
	@Test
	void test32() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 33: LEVEL: ")
	@Test
	void test33() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 34: LEVEL: ")
	@Test
	void test34() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 35: LEVEL: ")
	@Test
	void test35() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 36: LEVEL: ")
	@Test
	void test36() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 37: LEVEL: ")
	@Test
	void test37() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 38: LEVEL: ")
	@Test
	void test38() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 39: LEVEL: ")
	@Test
	void test39() {
		lvl = new Level();
		String test_ID = "^";
		String test_Colour = "y";
		String test_shape = lvl.setGridPattern(test_ID, test_Colour);
		String expected = "";
		String actual = test_shape;
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 40: LOADER: ")
	@Test
	void test40() {
		load = new Loader();
		int[] test_Player_value = load.getPlayer();
		int[] expected = {2,5};
		String actual = test_Player_value.toString();
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
	
	@DisplayName("Test 41: LOADER: ")
	@Test
	void test41() {
		load = new Loader();
		int[] test_Player_value = load.getPlayer();
		int[] expected = {2,5};
		String actual = test_Player_value.toString();
		String errormessage = "Expected " + expected + " But I got " + actual;
		assertEquals(errormessage, expected, actual);
	}
}
